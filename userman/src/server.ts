import * as express from 'express';
import * as Path from "path";

class User {
    id: number;
    username: string;
    password: string;
    email: string;

    constructor(id: number, username: string, password: string, email: string) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
    }
}

class Pet {
    id: number;
    name: string;
    kind: string;

    constructor(id: number, name: string, kind: string) {
        this.id = id;
        this.name = name;
        this.kind = kind;
    }
}

let nextUserId = 1;
let nextPetId = 1;

const users: Map<number, User> = new Map();
const pets: Map<number, Pet> = new Map();


const app: express.Express = express();
app.listen(8080);

app.use(express.json());
app.use(express.urlencoded({extended: false}));


app.get('/', (req: express.Request, res: express.Response) => {
    res.sendFile(Path.join(__dirname, '/userman/index.html'));
})

app.use("/ressources", express.static("public"));
app.use("/ressources/bootstrap", express.static("public/node_modules/bootstrap/dist/css"));

// Users Routen
app.get("/users", getAllUsers);
app.get("/users/:id", getUser);
app.post("/users", createUser);
app.put("/users/:id", updateUser);
app.delete("/users/:id", deleteUser);

// Pets Routen
app.get("/pets", getAllPets);
app.get("/pets/:id", getPet);
app.post("/pets", createPet);
app.put("/pets/:id", updatePet);
app.patch("/pets/:id", patchPet);
app.delete("/pets/:id", deletePet);


app.use(notFound);

// Users Funktionen
function getAllUsers(req: express.Request, res: express.Response): void {
    res.json(Array.from(users.values()));
}

function getUser(req: express.Request, res: express.Response): void {
    const id: number = parseInt(req.params.id);
    if (users.has(id)) {
        res.json(users.get(id));
    } else {
        res.status(404).send("User not found");
    }
}

function createUser(req: express.Request, res: express.Response): void {
    const { username, password, email } = req.body;
    if (!username || !password || !email) {
        res.status(400).send("Incomplete user data");
        return;
    }

    const id = nextUserId++;
    const newUser = new User(id, username, password, email);
    users.set(id, newUser);
    res.status(201).json(newUser);
}

function updateUser(req: express.Request, res: express.Response): void {
    const id: number = parseInt(req.params.id);
    const { username, password, email } = req.body;
    if (!users.has(id)) {
        res.status(404).send("User not found");
        return;
    }

    const userToUpdate = users.get(id);
    if (username) userToUpdate.username = username;
    if (password) userToUpdate.password = password;
    if (email) userToUpdate.email = email;

    res.json(userToUpdate);
}

function deleteUser(req: express.Request, res: express.Response): void {
    const id: number = parseInt(req.params.id);
    if (!users.has(id)) {
        res.status(404).send("User not found");
        return;
    }

    users.delete(id);
    res.sendStatus(204);
}

// Pets Funktionen
function getAllPets(req: express.Request, res: express.Response): void {
    res.json(Array.from(pets.values()));
}

function getPet(req: express.Request, res: express.Response): void {
    const id: number = parseInt(req.params.id);
    if (pets.has(id)) {
        res.json(pets.get(id));
    } else {
        res.status(404).send("Pet not found");
    }
}

function createPet(req: express.Request, res: express.Response): void {
    const { name, kind } = req.body;
    if (!name || !kind) {
        res.status(400).send("Incomplete pet data");
        return;
    }

    const id = nextPetId++;
    const newPet = new Pet(id, name, kind);
    pets.set(id, newPet);
    res.status(201).json(newPet);
}

function updatePet(req: express.Request, res: express.Response): void {
    const id: number = parseInt(req.params.id);
    const { name, kind } = req.body;
    if (!pets.has(id)) {
        res.status(404).send("Pet not found");
        return;
    }

    const petToUpdate = pets.get(id);
    if (name) petToUpdate.name = name;
    if (kind) petToUpdate.kind = kind;

    res.json(petToUpdate);
}

function patchPet(req: express.Request, res: express.Response): void {
    const id: number = parseInt(req.params.id);
    if (!pets.has(id)) {
        res.status(404).send("Pet not found");
        return;
    }

    const petToUpdate = pets.get(id);
    if (req.body.name) petToUpdate.name = req.body.name;
    if (req.body.kind) petToUpdate.kind = req.body.kind;

    res.json(petToUpdate);
}

function deletePet(req: express.Request, res: express.Response): void {
    const id: number = parseInt(req.params.id);
    if (!pets.has(id)) {
        res.status(404).send("Pet not found");
        return;
    }

    pets.delete(id);
    res.sendStatus(204);
}

function notFound(req: express.Request, res: express.Response): void {
    res.status(404).send("Die angeforderte Ressource wurde nicht gefunden");
}
