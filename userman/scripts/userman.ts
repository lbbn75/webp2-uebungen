/**
 * Definition der User-Klasse bestehend aus den Attributen Vor- und Zunamen, Emailadresse sowie Passwort.
 */
class User {
    firstname: string;
    lastname: string;
    password: string;
    mail: string;
    constructor(firstName: string, lastName: string, Mail: string, password: string) {
        this.firstname = firstName;
        this.lastname = lastName;
        this.mail = Mail;
        this.password = password;
    }
}

class Pet {
    name: string;
    kind: string;

    constructor(kind: string, name: string) {
        this.kind = kind;
        this.name = name;
    }
}

const pets: Map<string, Pet> = new Map<string, Pet>();
let kindInput: HTMLInputElement;
let nameInput: HTMLInputElement;
let petTable: HTMLElement;

document.addEventListener("DOMContentLoaded", () => {
    kindInput = document.querySelector("#kindInput");
    nameInput = document.querySelector("#nameInput");
    petTable = document.querySelector("#petTable");

    document.querySelector("#addPetForm").addEventListener("submit", (event: Event) => {
        event.preventDefault();
        addPet(new Pet(kindInput.value, nameInput.value));
        kindInput.value = ""; // Clear input fields after adding pet
        nameInput.value = "";
    });

    petTable.addEventListener("click", (event: Event) => {
        const target: HTMLElement = event.target as HTMLElement;
        if (target.matches("button.deletePet")) {
            const name: string = target.dataset.name;
            deletePet(name);
        } else if (target.matches("button.editPet")) {
            const name: string = target.dataset.name;
            startEditPet(name);
        }
    });
    getUsersFromServer();
    getPetsFromServer();
});

/**
 * Definition der globalen Variablen
 * Hier sind es die benötigten HTML-Elemente und das User-Array.
 */
let formEdit: HTMLFormElement;
let inputFirstName: HTMLInputElement;
let inputLastName: HTMLInputElement;
let inputmail: HTMLInputElement;
let inputPass: HTMLInputElement;
let inputEditFirstName: HTMLInputElement;
let inputEditLastName: HTMLInputElement;
let tableUser: HTMLElement;
const users: Map<string, User> = new Map<string, User>();

/**
 * Die Callback-Funktion initialisiert nach dem Laden des DOMs die globalen Variablen
 * und registriert die Eventhandler.
 */
document.addEventListener("DOMContentLoaded", () => { //Soll das noch mit DOMContentLoaded gemacht werden, oder mit defer im HTML-Head
    tableUser = document.querySelector("#tableUser");
    formEdit = document.querySelector("#formEdit");
    inputFirstName = document.querySelector("#formInput [name='firstname']");
    inputLastName = document.querySelector("#formInput [name='lastname']");
    inputmail = document.querySelector("#formInput [name='email']");
    inputPass = document.querySelector("#formInput [name='password']");
    inputEditFirstName = document.querySelector("#formEdit [name='firstname']");
    inputEditLastName = document.querySelector("#formEdit [name='lastname']");

    document.querySelector("#formInput").addEventListener("submit", addUser);
    formEdit.addEventListener("submit", editUser);
    document.querySelector("#editClose").addEventListener("click", stopEdit);
    tableUser.addEventListener("click", (event: Event) => {
        // Da das Klickziel die Tabelle an sich ist, muss das genaue Ziel im DOM noch bestimmt werden
        let target: HTMLElement = event.target as HTMLElement;
        target = target.closest("button");
        if (target.matches(".delete")) {
            deleteUser(target.dataset.mail); // Hier sollte target.dataset.email übergeben werden
        } else if (target.matches(".edit")) {
            startEdit(target);
        }
    });
});

// Beispiel für eine AJAX-Anfrage zum Abrufen aller Benutzer vom Server
function getUsersFromServer() {
    fetch('https://userman.thuermer.red/api/users', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            users.clear();
            data.forEach((user: any) => {
                users.set(user.mail, new User(user.firstname, user.lastname, user.password, user.mail));
            });
            renderUserList();
        })
        .catch(error => {
            console.error('Error fetching users:', error);
            // Behandeln Sie den Fehler hier, z. B. anzeigen einer Fehlermeldung für den Benutzer
        });
}

function getPetsFromServer(): void {
    fetch('https://userman.thuermer.red/api/users/0/pets')
        .then(response => response.json())
        .then((data: Pet[]) => {
            pets.clear();
            data.forEach((pet: Pet) => {
                pets.set(pet.name, pet);
            });
            renderPetList();
        })
        .catch(error => {
            console.error('Error fetching pets:', error);
        });
}


/**
 * Die Funktion liest die benötigten Werte aus den Inputfeldern.
 * Es wird ein neuer User erzeugt und der Map hinzugefügt.
 * @param event zum Unterdrücken des Standardverhaltens (Neuladen der Seite)
 */
function addUser(event: Event) {
    event.preventDefault();
    const user = new User(inputFirstName.value, inputLastName.value, inputmail.value, inputPass.value);
    addUserToServer(user);
}

function addUserToServer(user: User): void {
    fetch('https://userman.thuermer.red/api/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
    })
        .then(response => response.json())
        .then(() => {
            getUsersFromServer();
        })
        .catch(error => {
            console.error('Error adding user:', error);
        });
}


function addPet(pet: Pet): void {
    fetch('https://userman.thuermer.red/api/users/0/pets', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(pet),
    })
        .then(response => response.json())
        .then(() => {
            getPetsFromServer();
        })
        .catch(error => {
            console.error('Error adding pet:', error);
        });
}


/**
 * Die Funktion wird zu Beginn des Editiervorgangs aufgerufen.
 * Sie überträgt die Daten des aktuellen Elements in den Editierbereich und zeigt ihn an.
 * @param target das angeklickte Element
 */
function startEdit(target: HTMLElement) {
    const mail: string = target.dataset.mail;
    const user: User = users.get(mail);

    inputEditFirstName.value = user.firstname;
    inputEditLastName.value = user.lastname;
    formEdit.dataset.mail = mail;
    formEdit.style.display = "block";
}
function startEditPet(name: string): void {
    const pet: Pet = pets.get(name);
    kindInput.value = pet.kind;
    nameInput.value = pet.name;
    document.getElementById("addPetForm").style.display = "none";
    document.getElementById("editPetForm").style.display = "block";
    document.getElementById("editPetForm").setAttribute("data-pet-name", name);
}

function stopEdit() {
    formEdit.style.display = "none";
}

/**
 * Die Funktion wird aufgerufen, wenn das Editieren quittiert wird.
 * Die benötigten Felder werden ausgelesen und das Formular ausgeblendet.
 * @param event zum Unterdrücken des Standardverhaltens (Neuladen der Seite)
 */
function editUser(event: Event) {
    event.preventDefault();
    const mail: string = formEdit.dataset.mail;
    const user: User = users.get(mail);

    user.firstname = inputEditFirstName.value;
    user.lastname = inputEditLastName.value;
    formEdit.style.display = "none";
    updateUserOnServer(user);
}

function updateUserOnServer(user: User): void {
    fetch(`https://userman.thuermer.red/api/users/${user.mail}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
    })
        .then(response => response.json())
        .then(() => {
            getUsersFromServer();
        })
        .catch(error => {
            console.error('Error updating user:', error);
        });
}

function editPet(event: Event): void {
    event.preventDefault();
    // Hole den aktuellen Namen des Haustiers aus dem Datenattribut
    const currentName: string = document.getElementById("editPetForm").getAttribute("data-pet-name");
    // Hole die neuen Werte aus den Eingabefeldern
    const newName: string = nameInput.value;
    const newKind: string = kindInput.value;
    // Aktualisiere das Haustier in der Map
    const pet = pets.get(currentName);
    pet.name = newName;
    pet.kind = newKind;
    pets.delete(currentName);
    pets.set(newName, pet);
    // Verstecke das Bearbeitungsformular und zeige das Hinzufügeformular wieder an
    document.getElementById("addPetForm").style.display = "block";
    document.getElementById("editPetForm").style.display = "none";
    // Rendere die Haustierliste neu
    renderPetList();
}

document.addEventListener("DOMContentLoaded", () => {
    // Füge den Event-Listener für das Bearbeitungsformular hinzu
    document.getElementById("editPetForm").addEventListener("submit", editPet);
});
/**
 * Entfernt das aktuelle Element aus dem Array.
 * @param mail
 */
function deleteUser(mail: string): void {
    fetch(`https://userman.thuermer.red/api/users/${mail}`, {
        method: 'DELETE',
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to delete user');
            }
            // Benutzer erfolgreich gelöscht, jetzt die Benutzeroberfläche aktualisieren
            getUsersFromServer();
        })
        .catch(error => {
            // Fehler beim Löschen des Benutzers auf dem Server
            console.error('Error deleting user:', error);
        });
}


function deletePet(name: string): void {
    fetch(`https://userman.thuermer.red/api/users/0/pets${name}`, {
        method: 'DELETE',
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to delete pet');
            }
            getPetsFromServer();
        })
        .catch(error => {
            console.error('Error deleting pet:', error);
        });
}


/**
 * Löscht die Inhalte der Tabelle und baut sie auf Grundlage des Arrays neu auf.
 */
function renderUserList() {
    tableUser.innerHTML = "";

    for (const u of users.values()) {
        const tr: HTMLElement = document.createElement("tr");
        tr.innerHTML = `
            <td>${u.mail}</td>
            <td>${u.lastname}</td>
            <td>${u.firstname}</td>

            <td>
                 <button class="btn btn-primary delete" data-email="${u.mail}"><i class="fas fa-trash"></i></button>
                 <button class="btn btn-primary edit" data-email="${u.mail}"><i class="fas fa-pen"></i></button>
            </td>
        `;
        tableUser.append(tr);
    }
}

function renderPetList(): void {
    petTable.innerHTML = "";

    for (const pet of pets.values()) {
        const tr: HTMLElement = document.createElement("tr");
        tr.innerHTML = `
            <td>${pet.kind}</td>
            <td>${pet.name}</td>
            <td>
                <button class="btn btn-primary deletePet" data-name="${pet.name}">Löschen</button>
                <button class="btn btn-primary editPet" data-name="${pet.name}">Bearbeiten</button>
            </td>
        `;
        petTable.append(tr);
    }
}
